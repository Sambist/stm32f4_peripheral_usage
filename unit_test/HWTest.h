/*
 * HWTest.h
 *
 *  Created on: Aug 20, 2014
 *      Author: ser
 */

#ifndef HWTEST_H_
#define HWTEST_H_

class HWTest
{
public:
	static void usb_cdc_test (void);
	static void SDIOTest(void);
};

#endif /* HWTEST_H_ */
