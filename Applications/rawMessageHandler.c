/*
 * rawMessageHandler.c
 *
 *  Created on: Aug 25, 2014
 *      Author: ser
 */
#include "SystemConfig.h"

void (*funcx)(unsigned char*, void*);
void * ptrToQueuex;

void setRawPackage(void (*func)( unsigned char*, void*), void * ptrToQueue)
{
	funcx = func;
	ptrToQueuex = ptrToQueue;
}


void getRawByte(unsigned char* Input)
{
	(*funcx)(Input, ptrToQueuex);
}
