/*
 * CMainHandler.h
 *
 *  Created on: Aug 22, 2014
 *      Author: ser
 */

#ifndef CMAINHANDLER_H_
#define CMAINHANDLER_H_

#include "ff.h"
/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"

class CMainHandler {
	 xTaskHandle taskHandle;
	 const char *defString = "Default test string";
	 FIL inputFile, inputQueue;      /* File object */
public:
	void run();
	CMainHandler(const char *name, unsigned short stackDepth, char priority);
	void TaskSuspend();
	virtual ~CMainHandler();
};

#endif /* CMAINHANDLER_H_ */
