/*
 * rawMessageHandler.h
 *
 *  Created on: Aug 25, 2014
 *      Author: ser
 */

#ifndef RAWMESSAGEHANDLER_H_
#define RAWMESSAGEHANDLER_H_

#ifdef __cplusplus
extern "C" {
 #endif

void setRawPackage(void (*func)(unsigned char*, void*), void * ptrToQueue);
void getRawByte(unsigned char* Input);

#ifdef __cplusplus
}
#endif

#endif /* RAWMESSAGEHANDLER_H_ */
