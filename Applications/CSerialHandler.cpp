/*
 * CSerialHandler.cpp
 *
 *  Created on: Aug 22, 2014
 *      Author: ser
 */

#include "CSerialHandler.h"
#include "ClassMessage.h"
#include "HW.h"
#include "QueueBox.h"
#include "string.h"
#include "stdlib.h"

#define CompareSize		3
const char ActivationWord[5] = "ALCD";

extern "C" void pvSerialTaskCode(void *pvParameters) {
    (static_cast<CSerialHandler*>(pvParameters))->run();
}

CSerialHandler::CSerialHandler(const char *name, unsigned short stackDepth, char priority, xTaskHandle& parentHandler_)
{
	xTaskCreate(pvSerialTaskCode, (const char *) name, stackDepth, (void*) this, priority, &taskHandle);
	parentHandler = parentHandler_;
}

CSerialHandler::~CSerialHandler() {
	// TODO Auto-generated destructor stub
}

void CSerialHandler::run()
{
	ClassMessage InMessage;
	unsigned char InputBuffer[255];
	short length;
	while(1)
	{
		QueueBox::getIQueue().SerialQ.receive(InputBuffer);
		if (ClassMessage::LCDData == InputBuffer[ClassMessage::TYPE_POSITION] )
		{
			if(InMessage.getMessage(InputBuffer, &length) == OK)
			{
				vTaskSuspend(this->parentHandler);
				QueueBox::getIQueue().LCDQueue.send(InputBuffer);
			}
		}
		else if((ClassMessage::System == InputBuffer[ClassMessage::TYPE_POSITION]))
		{
			if(InMessage.getMessage(InputBuffer, &length) == OK)
			{
				if(!memcmp(InputBuffer,ActivationWord,CompareSize))
				{
					vTaskResume( this->parentHandler );
				}
			}
		}
		vTaskDelay(100);
	}
}

