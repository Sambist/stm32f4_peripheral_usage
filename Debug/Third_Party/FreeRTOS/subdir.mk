################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Third_Party/FreeRTOS/croutine.c \
../Third_Party/FreeRTOS/event_groups.c \
../Third_Party/FreeRTOS/heap_2.c \
../Third_Party/FreeRTOS/list.c \
../Third_Party/FreeRTOS/queue.c \
../Third_Party/FreeRTOS/tasks.c \
../Third_Party/FreeRTOS/timers.c 

OBJS += \
./Third_Party/FreeRTOS/croutine.o \
./Third_Party/FreeRTOS/event_groups.o \
./Third_Party/FreeRTOS/heap_2.o \
./Third_Party/FreeRTOS/list.o \
./Third_Party/FreeRTOS/queue.o \
./Third_Party/FreeRTOS/tasks.o \
./Third_Party/FreeRTOS/timers.o 

C_DEPS += \
./Third_Party/FreeRTOS/croutine.d \
./Third_Party/FreeRTOS/event_groups.d \
./Third_Party/FreeRTOS/heap_2.d \
./Third_Party/FreeRTOS/list.d \
./Third_Party/FreeRTOS/queue.d \
./Third_Party/FreeRTOS/tasks.d \
./Third_Party/FreeRTOS/timers.d 


# Each subdirectory must supply rules for building sources it contributes
Third_Party/FreeRTOS/%.o: ../Third_Party/FreeRTOS/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mthumb-interwork -mlittle-endian -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall -Wextra  -g -ggdb -DDEBUG -DSTM32F40_41xxx -DUSE_HAL_DRIVER -DHSE_VALUE=8000000 -D"assert_param(expr)= ((void)0)" -I"/home/data/Projects/DrumModule/WS/stm32f4/inc" -I"/home/data/Projects/DrumModule/WS/stm32f4/system/CMSIS/core" -I"/home/data/Projects/DrumModule/WS/stm32f4/system/CMSIS/stm32" -I"/home/data/Projects/DrumModule/WS/stm32f4/system/STM32F4xx_StdPeriph_Driver/inc" -I"/home/data/Projects/DrumModule/WS/stm32f4/Drivers" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/fat_fs/inc" -I"/home/data/Projects/DrumModule/WS/stm32f4/config" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/FreeRTOS/include" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/FreeRTOS/portable" -I"/home/data/Projects/DrumModule/WS/stm32f4/unit_test" -I"/home/data/Projects/DrumModule/WS/stm32f4/config/usb_config" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/USB_Lib/cdc" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/USB_Lib/core" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/USB_Lib/otg" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/fat_fs" -I"/home/data/Projects/DrumModule/WS/stm32f4/Applications" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


