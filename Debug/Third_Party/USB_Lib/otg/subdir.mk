################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Third_Party/USB_Lib/otg/usb_core.c \
../Third_Party/USB_Lib/otg/usb_dcd.c \
../Third_Party/USB_Lib/otg/usb_dcd_int.c 

OBJS += \
./Third_Party/USB_Lib/otg/usb_core.o \
./Third_Party/USB_Lib/otg/usb_dcd.o \
./Third_Party/USB_Lib/otg/usb_dcd_int.o 

C_DEPS += \
./Third_Party/USB_Lib/otg/usb_core.d \
./Third_Party/USB_Lib/otg/usb_dcd.d \
./Third_Party/USB_Lib/otg/usb_dcd_int.d 


# Each subdirectory must supply rules for building sources it contributes
Third_Party/USB_Lib/otg/%.o: ../Third_Party/USB_Lib/otg/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mthumb-interwork -mlittle-endian -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall -Wextra  -g -ggdb -DDEBUG -DSTM32F40_41xxx -DUSE_HAL_DRIVER -DHSE_VALUE=8000000 -D"assert_param(expr)= ((void)0)" -I"/home/data/Projects/DrumModule/WS/stm32f4/inc" -I"/home/data/Projects/DrumModule/WS/stm32f4/system/CMSIS/core" -I"/home/data/Projects/DrumModule/WS/stm32f4/system/CMSIS/stm32" -I"/home/data/Projects/DrumModule/WS/stm32f4/system/STM32F4xx_StdPeriph_Driver/inc" -I"/home/data/Projects/DrumModule/WS/stm32f4/Drivers" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/fat_fs/inc" -I"/home/data/Projects/DrumModule/WS/stm32f4/config" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/FreeRTOS/include" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/FreeRTOS/portable" -I"/home/data/Projects/DrumModule/WS/stm32f4/unit_test" -I"/home/data/Projects/DrumModule/WS/stm32f4/config/usb_config" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/USB_Lib/cdc" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/USB_Lib/core" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/USB_Lib/otg" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/fat_fs" -I"/home/data/Projects/DrumModule/WS/stm32f4/Applications" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


