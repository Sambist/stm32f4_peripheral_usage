/*
 * HWInitialization.h
 *
 *  Created on: Aug 21, 2014
 *      Author: ser
 */

#ifndef HWINITIALIZATION_H_
#define HWINITIALIZATION_H_

#include "SerialRS232.h"
#include "LCD_4002.h"
#include "BSPConfig.h"

class HW {

public:
	static HW& getHW(void);
	SerialRS232 SerialInterface;
	CLCD LCD4002;
private:
	HW();
	virtual ~HW();
};

#endif /* HWINITIALIZATION_H_ */
