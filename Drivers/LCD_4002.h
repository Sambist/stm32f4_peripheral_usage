/*
 * LCD_44780.h
 *
 *  Created on: Aug 21, 2014
 *      Author: ser
 */

#ifndef LCD_44780_H_
#define LCD_44780_H_

#include "stm32f4_discovery.h"


class CLCD{
public:
	CLCD();
	void Init(uint8_t CharInLine_);
	~CLCD();

	void LCD_Putchar (unsigned int DAT);
	void LCD_String (const char *STRING);
	void LCD_String (const char *STRING, char buf_size);
	void LCD_Goto (char str, char col);
	void LCD_Goto (uint32_t ADR);
	void LCD_Clear(void);
private:
	uint8_t CharInLine;

};

#endif /* LCD_44780_H_ */
